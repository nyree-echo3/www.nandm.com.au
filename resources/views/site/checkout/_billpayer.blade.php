<h3>Bill To</h3>

<div class="form-group form-group-sm row{{ $errors->has('billpayer.firstname') ? ' has-danger' : '' }}">
    <label class="form-control-label col-md-2">{{ __('First Name *') }}</label>
    <div class="col-md-10">
            {{ Form::text('billpayer[firstname]', null, [
                    'class' => 'form-control',
                    'placeholder' => __('First name')
                ])
            }}

            @if ($errors->has('billpayer.firstname'))
                <div class="form-control-feedback"><i class="fas fa-exclamation"></i>  First name is required<!--{{ $errors->first('billpayer.firstname') }}--></div>
            @endif
    </div>
</div>
  
<div class="form-group form-group-sm row{{ $errors->has('billpayer.lastname') ? ' has-danger' : '' }}">
    <label class="form-control-label col-md-2">{{ __('Last Name *') }}</label>
    <div class="col-md-10">
            {{ Form::text('billpayer[lastname]', null, [
                    'class' => 'form-control',
                    'placeholder' => __('Last name')
                ])
            }}

            @if ($errors->has('billpayer.lastname'))
                <div class="form-control-feedback"><i class="fas fa-exclamation"></i>  Last name is required<!--{{ $errors->first('billpayer.firstname') }}--></div>
            @endif
    </div>
</div>  

<div class="form-group form-group-sm row{{ $errors->has('billpayer.email') ? ' has-danger' : '' }}">
    <label class="form-control-label col-md-2">{{ __('Email *') }}</label>
    <div class="col-md-10">
            {{ Form::text('billpayer[email]', null, [
                    'class' => 'form-control',
                    'placeholder' => __('Email')
                ])
            }}

            @if ($errors->has('billpayer.email'))
                <div class="form-control-feedback"><i class="fas fa-exclamation"></i>  Email is required<!--{{ $errors->first('billpayer.firstname') }}--></div>
            @endif
    </div>
</div>

<!--
<div class="form-group form-group-sm{{ $errors->has('type') ? ' has-danger' : '' }}">
    <div class="checkbox">
        <label>
            {{ Form::checkbox('billpayer[is_organization]', 1, null, [
                'v-model' => 'isOrganization'
                ])
            }}
            {{ __('Bill to Company') }}
        </label>
    </div>

    @if ($errors->has('type'))
        <div class="form-control-feedback">{{ $errors->first('type') }}</div>
    @endif
</div>

<div id="billpayer-organization" v-show="isOrganization">
    <div class="form-group form-group-sm{{ $errors->has('billpayer.company_name') ? ' has-danger' : '' }}">
        {{ Form::text('billpayer[company_name]', null, ['class' => 'form-control form-control-lg', 'placeholder' => __('Company name')]) }}
        @if ($errors->has('billpayer.company_name'))
            <div class="form-control-feedback">{{ $errors->first('billpayer.company_name') }}</div>
        @endif
    </div>

    <div class="form-group form-group-sm{{ $errors->has('billpayer.tax_nr') ? ' has-danger' : '' }}">
        {{ Form::text('billpayer[tax_nr]', null, ['class' => 'form-control', 'placeholder' => __('Tax no.')]) }}
        @if ($errors->has('billpayer.tax_nr'))
            <div class="form-control-feedback">{{ $errors->first('billpayer.tax_nr') }}</div>
        @endif
    </div>
</div>
-->

@include('site.checkout._billing_address', ['address' => $billpayer->getBillingAddress()])


@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')

<div class="panel">
	@include('site/partials/index-pages')	
</div>
    
@endsection

@if(isset($home_products))
	 <div class="home-products">
	   <div class="container">
		  <div class="row">         
			 @foreach($home_products as $item)       	 
				  <div class="col-lg-4">
			           <h2>{{ $item->name }}</h2>
			           {!! $item->excerpt !!}
			           
				       @if (count($item->images) > 0)	
				          <div class="home-products-img">
					         <img class="rounded-circle" src="{{ url('') }}/{{$item->images[0]->location}}" alt="{{ $item->name }}" width="140" height="140" />
					      </div>
					   @endif					   					 
					   					   
					   <p><a class="btn btn-secondary" href="{{ url('') }}/products/{{ $item->category->slug }}/{{ $item->slug }}" role="button">View details &raquo;</a></p>
				  </div><!-- /.col-lg-4 -->
			 @endforeach 	

			</div>
	   </div>
	</div>
@endif
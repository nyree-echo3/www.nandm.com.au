@extends('admin/layouts/app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Testimonials</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="far fa-comments"></i> Testimonials</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <form method="post" action="{{ url('dreamcms/testimonials') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">                           
                            <div class="form-group col-xs-2">
                                <input type="text" class="form-control" name="search" placeholder="Search" value="{{ $session['search'] }}">
                            </div>

                            <div class="form-group col-xs-3 filter-button">
                                <button type="submit" class="btn btn-info">Filter</button>
                                @if($is_filtered)
                                    <a href="{{ url('dreamcms/testimonials/forget') }}" type="submit" class="btn btn-danger">Remove</a>
                                @endif
                            </div>
                        </div>
                    </form>

                    <div class="pull-right box-tools">
                        @can('sort-testimonial')
                        <a href="{{ url('dreamcms/testimonials/sort') }}" type="button" class="btn bg-olive btn-sm"
                           data-widget="add">Sort
                            <i class="fa fa-list-ul"></i>
                        </a>
                        @endcan

                        @can('add-testimonials')
                        <a href="{{ url('dreamcms/testimonials/add') }}" type="button" class="btn btn-info btn-sm"
                           data-widget="add">Add New
                            <i class="fa fa-plus"></i>
                        </a>
                        @endcan
                    </div>
                </div>
                <div class="box-body">
                    @if(count($testimonials))
                        <table class="table table-hover">
                            <tr>
                                <th>@sortablelink('title')</th>                                
                                <th>@sortablelink('status')</th>
                                <th class="hd-text-right">Actions</th>
                            </tr>
                            @foreach($testimonials as $testimonial)
                                <tr>
                                    <td>{{ $testimonial->person }}</td>                                   
                                    <td>
                                        <input id="testimonial_{{ $testimonial->id }}" data-id="{{ $testimonial->id }}" class="testimonial_status" type="checkbox" data-toggle="toggle" data-size="mini"{{ $testimonial->status == 'active' ? ' checked' : null }}>
                                    </td>
                                    <td>
                                        <div class="pull-right">
                                            @can('edit-testimonials')
                                            <a href="{{ url('dreamcms/testimonials/'.$testimonial->id.'/edit') }}"
                                               class="tool" data-toggle="tooltip" title="Edit"><i
                                                        class="fa fa-edit"></i></a>
                                                        
                                            <a href="{{ url('dreamcms/testimonials/'.$testimonial->id.'/preview') }}" class="tool" data-toggle="tooltip" title="Preview" target="_blank"><i class="far fa-eye"></i></a>    
                                            @endcan

                                            @can('delete-testimonials')
                                            <a href="{{ url('dreamcms/testimonials/'.$testimonial->id.'/delete') }}"
                                               class="tool" data-toggle=confirmation data-title="Are you sure?"
                                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                               data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                           @endcan
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
                <div class="box-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <form id="pagination_count_form" method="post" class="form-inline" action="{{ url('dreamcms/pagination') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <select id="pagination_count" name="pagination_count">
                                    <option value="25"{{ Session::get('pagination-count')==25 ? ' selected="selected"' : '' }}>25</option>
                                    <option value="50"{{ Session::get('pagination-count')==50 ? ' selected="selected"' : '' }}>50</option>
                                    <option value="100"{{ Session::get('pagination-count')==100 ? ' selected="selected"' : '' }}>100</option>
                                </select>
                                <span class="total-row"> Total {{ $testimonials->total() }} record</span>
                            </form>
                        </div>
                        <div class="col-xs-6" style="text-align: right;">
                            {{ $testimonials->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
    <script src="{{ asset('/components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".select2").select2();

            $("#pagination_count").select2({
                minimumResultsForSearch: -1
            });

            $("#pagination_count").change(function() {
                $("#pagination_count_form").submit();
            });

            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });

            $('.testimonial_status').change(function() {
                $.ajax({
                    type: "POST",
                    url: "testimonials/"+$(this).data('id')+"/change-testimonial-status",
                    data:  {
                        'status':$(this).prop('checked')
                    },
                    success: function (response) {
                        if(response.status=="success"){
                            toastr.options = {"closeButton": true}
                            toastr.success('Status has been changed');
                        }
                    }
                });
            });

        });
    </script>
@endsection
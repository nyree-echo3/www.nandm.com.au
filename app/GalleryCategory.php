<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class GalleryCategory extends Model
{
    use SortableTrait;

    protected $table = 'gallery_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function images()
    {
        return $this->hasMany(Images::class, 'category_id');
    }
}
